var ENGINE_EXECUTE = ENGINE_EXECUTE || {};

ENGINE_EXECUTE.init = function(){

	var _headerH = null;

	return{
		execute: function(){
			this.engineSettingExecution();
			this.indexVideoExecution();
			this.globalCurrentExecution();
			this.personNavigatonExecution();
		},

		/*
		** ▼ ページ全体で使用する動き
		*/
		engineSettingExecution: function(){
			(function(){

				/* アンカーリンク */
				// $('a[href^=#]').click(function() {
				// 	var href= $(this).attr("href");
				// 	var target = $(href == "#" || href == "" ? 'html' : href);
				// 	var position = parseInt((target.offset().top)-_headerH);
				//
				// 	$('body,html').animate({scrollTop:position}, 1000, 'easeOutQuart');
				// 	return false;
				// });

				/* ページトップ */
				// $('.js_pageTop a').click(function() {
				// 	$('body,html').animate({scrollTop:0}, 800, 'easeOutQuart');
				// 	return false;
				// });

				/* 要素の高さ揃え */
				$('.js_alignHeight').matchHeight();

				$('.js_alignHeight_1').matchHeight();
				$('.js_alignHeight_2').matchHeight();
				$('.js_alignHeight_3').matchHeight();
				$('.js_alignHeight_4').matchHeight();
				$('.js_alignHeight_5').matchHeight();

				/* 特定のmoduleパーツの高さ揃え */
				// var $js_list_package = $('.md_list_package');
				// $js_list_package.find('.md_list_package_subTitle').matchHeight();
				// $js_list_package.find('.md_list_tag').matchHeight();
				// $js_list_package.find('.md_list_package_text').matchHeight();

				/* 電話番号PC版非作動 */
				var numberUa = navigator.userAgent.toLowerCase();
				var numberIsMobile = /iphone/.test(numberUa)||/android(.+)?mobile/.test(numberUa);

				if (!numberIsMobile) {
			    $('a[href^="tel:"]').css('cursor','default').on('click', function(e) {
			        e.preventDefault();
			    });
				}

			}());
		},

		/*
		** ▽ VIDEO
		*/
		indexVideoExecution: function(){
			var $videoIframe = null;

			var $videoPlayBtn = $('.js_videoLaunch_button');
			var $videoMonitor = $('#js_videoMonitor');
			var $videoMonitorInner = $videoMonitor.find('.md_modalVideo_inner');
			var $videoCloseBtn = $('#js_closeBtn');
			var $videoCloseObject = $('.closeObject');
			var _movieID = null;

			$videoPlayBtn.on('click', function(){
				_movieID = $(this).data('id');
				openIframe();
			});

			$videoCloseBtn.on('click', function(){
				closeIframe();
			});

			$videoCloseObject.on('click', function(){
				closeIframe();
			});

			// 動画再生
			function openIframe(){
				var obj = {};
				obj.ytid = _movieID;
				_html = '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/' + obj.ytid + '?autoplay=1&rel=0" frameborder="0" allowfullscreen></iframe>'
				$videoMonitorInner.prepend(_html);
				$videoMonitor.fadeIn();
				$videoIframe = $videoMonitor.find('iframe');
			}

			function closeIframe(){
				$videoMonitor.fadeOut();
				$videoIframe.remove();
			}
		},

		/*
		** ▽ グローバルメニューのカレント
		*/
		globalCurrentExecution: function(){
			var $js_globalMenu = $('.js_gMenu')
			var $dir = location.pathname.split("/");
			$dir = $.grep($dir, function(e){
				return e !== "";
			});
			var $targetDir = $dir[1];
			if($targetDir){
				$('.js_gMenu').find('.js_gMenu_' + $targetDir).addClass('on');
			}
			else{
				return false;
			}
		},

		/*
		** ▽ 社員紹介のローカルナビ制御
		*/
		personNavigatonExecution: function(){
			var $navigation = $('.uq_person_navigation');
			var __fileId = window.location.pathname.split('/');
			__fileId = __fileId[__fileId.length-1];
			__fileId = __fileId.split('.')[0];
			$navigation.find('.id_' + __fileId).addClass('on');
		}
	}
}

$(function(){
	ENGINE_EXECUTE.init().execute();
});

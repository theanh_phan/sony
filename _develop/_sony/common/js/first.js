var reTop = (function(){

	var now = 0;
	var imgCount;
	var aspectRatio = 1400 / 797;
	var bodyW,bodyH;

	function _init()
	{
		fadeInit();
		setTimeout(function(){
			fadeAction();
			setInterval(fadeAction, 5000);
		}, 4000);
		resizeWindow();
		$(window).resize(resizeWindow);
		setTimeout(function(){resizeWindow()},500);

		/* IE8用png対応 */
		if(navigator.userAgent.indexOf("MSIE") != -1) {
			$('#main .btm a.alpha img').each(function() {
				if($(this).attr('src').indexOf('.png') != -1) {
					$(this).css({
						'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src="' +
						$(this).attr('src') +
						'", sizingMethod="scale");'
					});
				}
			});
		}
	}

	function fadeInit() {
		$('#main_img li img').css('opacity', 0);
		$('#main_img li img').eq(0).css('opacity', 1);
		imgCount = $('#main_img li img').length;
	}

	function fadeAction() {
		now++;
		if(now == imgCount) now = 0;

		$('#main_img li img').animate({'opacity': 0}, 2000);
		$('#main_img li img').eq(now).stop().animate({'opacity': 1}, 2000);
	}

	function resizeWindow() {
		mainW = $(window).width();
		mainH = $(window).height();

		$('#main_img').css({'height': mainH + 'px'});
		$('#main_img li img').css({'height': mainH + 'px', 'width': 'auto'});

		if(mainW / mainH > aspectRatio) {
			$('#main_img li img').css({'height': 'auto', 'width': mainW + 'px'});
			var posY = -(($('#main_img li img').height() - mainH) / 2);
			$('#main_img li').css('top', posY + 'px');
		} else {
			$('#main_img li').css('top', '0px');
		}
	}

	return {
		init : _init
	}

}());

$(function(){
	reTop.init();
});

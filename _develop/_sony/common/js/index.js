var $js_movieObject = $('#mainPlayer');

$(function(){
  var mainMovUa = navigator.userAgent;
  if (mainMovUa.indexOf('iPhone') > 0 || mainMovUa.indexOf('iPod') > 0 || mainMovUa.indexOf('Android') > 0 && mainMovUa.indexOf('Mobile') > 0) {
    $js_movieObject.remove();
  } else if (mainMovUa.indexOf('iPad') > 0 || mainMovUa.indexOf('Android') > 0) {
    //alert("タブレット");
  } else {
    //$js_movieObject.remove();デバック用
    //alert("PC");
  }
});

// IFrame Player API の読み込み
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// YouTubeの埋め込み
var ytPlayer;
function onYouTubeIframeAPIReady() {
  ytPlayer = new YT.Player(
    'mainPlayer', // 埋め込む場所の指定
    {
      videoId: 'zggmLlPwN7U', // YouTubeのID
      wmode: 'transparent',
      playerVars: {
        'wmode': 'transparent',
        autoplay: 1, // 自動再生の設定
        loop: 1, // ループの設定
        playlist: 'zggmLlPwN7U', // 再生する動画のリスト
        controls: 0, // コントロールバーを表示しない
        showinfo: 0 // 動画情報を表示しない
      },
      events: {
        'onReady': onPlayerReady
      }
    }
  );
}

// プレーヤーの準備ができたとき
function onPlayerReady(event) {
  // 動画をミュートにする
  event.target.mute();
}

$(function() {
  // 背景動画の指定
  var bgMovie = $('.background-movie');

  // 背景動画のサイズ調整
  function background_size_cover(aspectW, aspectH) {
    // 画面サイズの取得
    var windowWidth = $(window).width();
    var windowHeight = $('uq_index_main').height();

    // 画面サイズの比率を引数で設定した比率と比較
    if(windowWidth / aspectW * aspectH > windowHeight) {
      // 画面の比率が縦長の場合
      // 縦横比を保持して、画面を完全に覆う最小サイズと配置位置を取得
      var bgWidth = windowWidth;
      var bgHeight = Math.floor(bgWidth / aspectW * aspectH);
      var bgTop = Math.floor(bgHeight / 2);
      var bgLeft = 0;
    } else {
      // 画面の比率が横長の場合
      // 縦横比を保持して、画面を完全に覆う最小サイズと配置位置を取得
      var bgHeight = windowHeight;
      var bgWidth = Math.floor(bgHeight / aspectH * aspectW);
      var bgTop = 0;
      var bgLeft = (windowWidth - bgWidth) / 2;
    }

    // 動画のサイズと位置の指定
    bgMovie.css({
      top: '50%',
      left: bgLeft,
      width: bgWidth,
      height: bgHeight,
			marginTop: '-' + bgTop + 'px'
    })
  }
  background_size_cover(16, 9);

  // リサイズ時に画像調整を実行
  $(window).on('resize', function() {
    background_size_cover(16, 9);
  });
});

$(function(){

  $(".accordion p").click(function(){
      $(this).toggleClass("menu_close");
      $(this).next("ul").slideToggle(220);
      $(this).children("span").toggleClass("open");
  });

  $(".accordion dt").click(function(){
      $(this).next("dd").slideToggle();
      $(this).next("dd").siblings("dd").slideUp();
      $(this).toggleClass("open");
      $(this).siblings("dt").removeClass("open");
  });

  $(".accordion p").keypress(function(){
      $(this).toggleClass("menu_close");
      $(this).next("ul").slideToggle(220);
      $(this).children("span").toggleClass("open");
  });

  $(".accordion dt").keypress(function(){
      $(this).next("dd").slideToggle();
      $(this).next("dd").siblings("dd").slideUp();
      $(this).toggleClass("open");
      $(this).siblings("dt").removeClass("open");
  });

  /*
  ** ▼ヘッダー調整追記
  */
  js_headerControlExecute();
  function js_headerControlExecute(){

    var $jhc_url = location.href.split("/");
    var $jhc_id = $jhc_url[$jhc_url.length -2];

    var $jhc_header = $('#header');
    var $jhc_l_header = $('.l_header');
    var $jhc_navigation = $('.l_headerSecond');
    var $jhc_container = $('#container');
    var $jhc_offset = $jhc_navigation.offset().top;

    var $jhc_width = $(window).width();


    

    $(window).scroll(function(){
      //console.log($jhc_offset);
      if($(window).scrollTop() > $jhc_offset){
        $jhc_header.css('position','fixed');
        $jhc_l_header.css('display','none');
        $jhc_container.addClass("is_fixed");
      }
      else
      {
        $jhc_header.css('position','static');
        $jhc_l_header.css('display','block');
        $jhc_container.removeClass("is_fixed");
      }
    });

    // ▼TOPページは処理しない
    // if($jhc_id !== "recruit"){
    //
    //   $(window).scroll(function(){
    //     //console.log($jhc_offset);
    //     if($(window).scrollTop() > $jhc_offset){
    //       $jhc_header.css('position','fixed');
    //       $jhc_l_header.css('display','none');
    //       $jhc_container.addClass("is_fixed");
    //     }
    //     else
    //     {
    //       $jhc_header.css('position','static');
    //       $jhc_l_header.css('display','block');
    //       $jhc_container.removeClass("is_fixed");
    //     }
    //   });
    // }
  }

});

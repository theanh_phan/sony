$(function() {
	var images = $("img");
	for(var i=0; i < images.size(); i++) {
		if(images.eq(i).attr("src").match("_btn.")) {
			$("img").eq(i).hover(function() {
				$(this).css('opacity', '0.6');
			}, function() {
				$(this).css('opacity', '1');
			});
		}
	}
});


$(function(){
 $('#navigation a').animateToSelector({
  selectors: ['#navigation a:hover'],
    properties: [
            'background-color',
            'padding-left',
            'color'
            ],
         duration:500,
        events: ['mouseover', 'mouseout']
         });
     });


function setRollOver() {
	var loadedImg = new Array();
	if (!document.getElementsByTagName) return false;
	var ovrImgList = document.getElementsByTagName('img');
	for (var i = 0; i < ovrImgList.length; i++) {
		if (ovrImgList[i].src.match(/_off\./i)) {
			loadedImg[i] = new Image();
			loadedImg[i].src = ovrImgList[i].src.replace(/_off\./i, '_over.');
			ovrImgList[i].onmouseover = function() {
				this.src = this.src.replace(/_off\./i, '_over.');
			}
			ovrImgList[i].onmouseout = function() {
				this.src = this.src.replace(/_over\./i, '_off.');
			}
			if (navigator.userAgent.indexOf('MSIE') < 0) ovrImgList[i].onmouseup = function() {
				this.src = this.src.replace(/_over\./i, '_off.');
			}
		}
	}
	return true;
}
if (window.addEventListener) window.addEventListener('load', setRollOver, false);
if (window.attachEvent) window.attachEvent('onload', setRollOver);


var lastURL1 = "img/detail/d_0.jpg";

function setImage1(imageURL)
{
	lastURL1 = imageURL;
	document.viewFrame1.src = imageURL;
}

function reImage1()
{
	document.viewFrame1.src = lastURL1;
}

$(function(){
	$(window).bind("load resize", init);
	function init(){

		var _width = $(window).width();//デバイス（ウィンドウ）幅を取得
		if(_width <= 1200){

			var nav    = $('#index_header'),
			    offset = nav.offset(),
			    subcon = $('#sub_contents');


			$(window).scroll(function () {
			  if($(window).scrollTop() > offset.top) {
			    nav.addClass('fixed');
			    subcon.addClass('margin');
			  } else {
			    nav.removeClass('fixed');
			    subcon.removeClass('margin');
			  }

			});
		}else{

			var nav    = $('#index_header'),
			    offset = nav.offset(),
			    subcon = $('#sub_contents'),
			    nav2   = $('#pc_entry_nav');

			$(window).scroll(function () {
			  if($(window).scrollTop() > offset.top) {
			    nav.addClass('fixed');
			    subcon.addClass('margin');
			    nav2.addClass('btn_on');
			  } else {
			    nav.removeClass('fixed');
			    subcon.removeClass('margin');
			    nav2.removeClass('btn_on');
			  }

			});
		}
	}//init
});

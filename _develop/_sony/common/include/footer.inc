<footer id="footer" class="footer">
	<div class="container">
		<div class="footer-inner">
			<div class="footer-info">
				<h3>オーエス産業株式会社</h3>
				<p>〒670-0804　姫路市保城津倉188<br>
				TEL ： 079-281-1530<br>
				FAX ： 079-288-0928</p>
			</div>
			<div class="footer-nav">
				<ul>
					<li><a href="/">HOME</a></li>
				</ul>
				<ul>
					<li><a href="/advantage/">オーエスの強み</a></li>
					<li><a href="/advantage/development.html">開発体制・技術力</a></li>
					<li><a href="/advantage/support.html">サポート・メンテナンス</a></li>
					<li><a href="/advantage/quality.html">品質・安全への取り組み</a></li>
				</ul>
				<ul>
					<li><a href="/company/">ごあいさつ</a></li>
					<li><a href="/company/outline.html">会社概要</a></li>
					<li><a href="/company/organization.html">組織図</a></li>
					<li><a href="/company/factory.html">工場設備</a></li>
				</ul>
				<ul>
					<li><a href="/product/">製品一覧</a></li>
					<li><a href="/product/ceiling.html">天井クレーン</a></li>
					<li><a href="/product/bridge.html">橋形クレーン</a></li>
					<li><a href="/product/special.html">特殊クレーン</a></li>
					<li><a href="/product/other.html">その他製品</a></li>
				</ul>
				<ul>
					<li><a href="/product/data01.html">天井クレーン使用詳細</a></li>
					<li><a href="/product/rail.html">走行レール敷設</a></li>
					<li><a href="/product/building.html">建屋内設置寸法</a></li>
					<li><a href="/product/ladder.html">はしご道寸法</a></li>
					<li><a href="/product/runway.html">ウレタン車輪用ランウェイ<br>ガーダ設置注意事項</a></li>
				</ul>
				<ul>
					<li><a href="/contact/">お問い合わせ</a></li>
					<li><a href="/contact/form_contact/">お問い合わせフォーム</a></li>
					<li><a href="/contact/form_estimate/">お見積りフォーム</a></li>
				</ul>
				<ul>
					<li><a href="/privacy/">個人情報保護方針</a></li>
					<li><a href="/sitemap/">サイトマップ</a></li>
				</ul>
			</div>
		</div>
		<small class="copyright">Copyright &copy; OS SANGYO Co., Ltd. All Rights Reserve</small>
	</div>
</footer>
<!-- end footer -->
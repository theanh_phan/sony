<header id="header" class="header">
	<div class="container">
		<h1 class="site-logo">
			<a href="/"><img src="/common/img/logo.png" alt="オーエス産業株式会社"></a>
		</h1>
		<nav class="gnav">
			<ul>
				<li><a href="/advantage/">オーエスの強み<span>ADVANTAGE</span></a></li>
				<li><a href="/product/">製品情報<span>PRODUCT</span></a></li>
				<li><a href="/company/">企業情報<span>COMPANY</span></a></li>
				<li><a href="/contact/">お問い合わせ<span>CONTACT</span></a></li>
				<li><a href="/recruit/">採用情報<span>RECRUIT</span></a></li>
			</ul>
		</nav>
		<div id="hambuger">
			<div class="nav-icon">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
		</div>
	</div>
</header>
<!-- end header -->
<nav class="gnav_sp">
	<ul>
		<li><a href="/">HOME</a></li>
		<li>
			<a href="/advantage/">オーエスの強み</a>
			<ul>
				<li><a href="/advantage/development.html">開発体制・技術力</a></li>
				<li><a href="/advantage/support.html">サポート・メンテナンス</a></li>
				<li><a href="/advantage/quality.html">品質・安全への取り組み</a></li>
			</ul>
		</li>
		<li>
			<span>製品情報</span>
			<ul>
				<li><a href="/product/">製品一覧</a></li>
				<li><a href="/product/ceiling.html">天井クレーン</a></li>
				<li><a href="/product/bridge.html">橋形クレーン</a></li>
				<li><a href="/product/special.html">特殊クレーン</a></li>
				<li><a href="/product/other.html">その他製品</a></li>
				<li><a href="/product/data01.html">天井クレーン使用詳細</a></li>
				<li><a href="/product/rail.html">走行レール敷設</a></li>
				<li><a href="/product/building.html">建屋内設置寸法</a></li>
				<li><a href="/product/ladder.html">はしご道寸法</a></li>
				<li><a href="/product/runway.html">ウレタン車輪用ランウェイガーダ設置注意事項</a></li>
			</ul>
		</li>
		<li>
			<span>企業情報</span>
			<ul>
				<li><a href="/company/">ごあいさつ</a></li>
				<li><a href="/company/outline.html">会社概要</a></li>
				<li><a href="/company/organization.html">組織図</a></li>
				<li><a href="/company/factory.html">工場設備</a></li>
			</ul>
		</li>
		<li>
			<a href="/contact/">お問い合わせ</a>
			<ul>
				<li><a href="/contact/form_contact/">お問い合わせフォーム</a></li>
				<li><a href="/contact/form_estimate/">お見積りフォーム</a></li>
			</ul>
		</li>
		<li>
			<span>その他</span>
			<ul>
				<li><a href="/privacy/">個人情報保護方針</a></li>
				<li><a href="/sitemap/">サイトマップ</a></li>
			</ul>
		</li>
	</ul>
	<div class="nav-close">
		<span>閉じる</span>
	</div>
</nav>